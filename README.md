# BL:REdistributables

Miscellaneous Steam-related DLLs necessary for BL:RE authentication purposes.

This repo runs a pipeline that fetches the DLLs from a specific Steam depot (currently [Arma 3 Dedicated Server](https://steamdb.info/depot/233782/)) and publishes them to [Gitlab's Package Registry](https://gitlab.com/northamp/blredistributables/-/packages).

It should also contain part of the Steam manifest pulled at the same time as the files formatted for `sha1sum` consumption.
